# Python development environment
This docker image contains all mandatory tools for python development into Fairplay.

# Tools
- [python 3.11.x](https://gitlab.com/fairplay2/open-source/docker/python)
- [wget](https://www.gnu.org/software/wget/)
- [vim](https://www.vim.org/)
- [zsh](https://www.zsh.org/)
- [oh-my-shell](https://ohmyz.sh/)
- [git](https://git-scm.com/)
- [poetry](https://python-poetry.org/)
- [pre-commit](https://pre-commit.com/)
- [make](https://www.computerhope.com/unix/umake.htm)

# How can i use this app?

## Docker

Download the image:
```bash
docker pull registry.gitlab.com/fairplay2/open-source/docker/python-local-environment:3.11
```

Enter to the image:
```bash
docker run -it registry.gitlab.com/fairplay2/open-source/docker/python-local-environment:3.11 zsh
```

Thats it, you will have access to the python environment. Remember this command is volatile that means, if you exit from the image, all the changes will be erased if you want to add persistence you need to add a `volume` or use `docker compose`

## Docker compose  (comming soon)

## Maintainers
<table>
    <tr>
        <td align="center">
            <a href="https://gitlab.com/dharwinfairplay">
                <img src="https://gitlab.com/uploads/-/system/user/avatar/12814508/avatar.png?width=400" width="100px;" alt="Dharwin Perez"/>
                <br />
                <sub>
                    <b>Dharwin Perez</b>
                </sub>
            </a>
        </td>
    </tr>
</table>

## Contributors ✨
<table>
    <tr>
        <!-- <td align="center">
            <a href="https://gitlab.com/dharwinfairplay">
                <img src="https://gitlab.com/uploads/-/system/user/avatar/12814508/avatar.png?width=400" width="100px;" alt="Dharwin Perez"/>
                <br />
                <sub>
                    <b>Dharwin Perez</b>
                </sub>
            </a>
        </td> -->
    </tr>
</table>