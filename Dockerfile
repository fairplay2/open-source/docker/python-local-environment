FROM registry.gitlab.com/fairplay2/open-source/docker/python:3.11

# Install apt packages
RUN apt-get update && apt-get install --no-install-recommends -y \
  git \
  # zsh for development
  zsh \
  # vim
  vim \
  # curl
  curl \
  wget \
  fonts-powerline \
  # cleaning up unused files
  && apt-get purge -y --auto-remove -o APT::AutoRemove::RecommendsImportant=false \
  && rm -rf /var/lib/apt/lists/*

# Install Oh My Shell
RUN sh -c "$(wget -O- https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
COPY ./configs/zshrc /root/.zshrc

# Install powerlevel10k theme
RUN git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/themes/powerlevel10k
COPY ./configs/p10k.zsh /root/.p10k.zsh